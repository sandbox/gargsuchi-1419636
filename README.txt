This module enables user to Recommend nodes or comments.
The best part about this module is that it user AJAX for recommend counts and actual recommend process. As a reuslt of this, we can use this module with external cahching enabled (like varnish) and it will still work fine.

Intallation
------------
1. Enable the module 
2. go to the settings page and do the required settings. On this page, you can decide wether u want to use Recommend for nodes or for comments or for both. You can also change the text that you need to show during the recommend process.


Varnish
--------
For making sure that this module works fine with varnish, you will need to include the following lines in your .vcl file

 if (req.url ~ "get_recommend|recommend") {
    return (pass);
  }
