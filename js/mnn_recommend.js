function get_recommend(content_type, content_id, base_url) {
	var url= base_url + "/get_recommend/" + content_type + "/" + content_id;
	
	$(".throbber").show();
	$.ajax({
		url: url,
		dataType: 'text',
		context: document.body,
		success: function(data){
			var class_recommend = '.mnn_recommend_' + content_type + "_" + content_id;
			  $(class_recommend).html(data);
			  $(".throbber").hide();
		  }
	});
};
function recommend(content_type, content_id, base_url) {
	var url= base_url + "/recommend/" + content_type + "/" + content_id;
	
	$(".throbber").show();
	$.ajax({
		url: url,
		dataType: 'text',
		context: document.body,
		success: function(data){
			var class_recommend = '.mnn_recommend_' + content_type + "_" + content_id;
			  $(class_recommend).html(data);
			  $(".throbber").hide();
		  }
	});
};
